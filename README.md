# ProjetFinEtude


Fr : ProjetFinEtude est un site internet qui gére l’organisation et la décoration des évènements (mariage, anniversaire, baptême, conférence) selon le calendrier, de la mise en location de matériels pour ces évènements.Ses collaborateurs sont généralement des agents de service traiteur, salon de coiffure.

En : ProjetFinEtude is a website that manages the organization and decoration of events (wedding, birthday, baptism, conference) according to the calendar, the rental of equipment for these events. Its employees are generally catering service agents, salon hairstyle.

## Development environment / Environnement de développement

### Pre-requirement / Pré-requis

* PHP 8.0
* MySQL 8.0
* Symfony CLI 5.4

En : you can check the pre-requirement with the following command (from the Symfony CLI):

Fr : Vous pouvez vérifier la pré-requis avec la commande suivante (de la CLI Symfony):

```bash
symfony book:check-requirements
symfony check:requirements
```

### Launch the working environment / lancer l'environnement de développement

```bash
symfony serve -d
 symfony open:local
```
