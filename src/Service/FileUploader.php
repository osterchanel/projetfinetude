<?php

namespace App\Service;

use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\Annotation\Target;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Entity\FileImage;

/**
 * @Annotation
 * @Target("CLASS")
 * @param string $targetDirectory -- Upload directory
 * @param SluggerInterface $slugger -- to create sluged name
 * @return FileImage $fileEntity
 */
class FileUploader
{
    /** @var SluggerInterface $slugger */
    private $slugger;

    /** @var ParameterBagInterface $params */
    private $params;

    public function __construct(SluggerInterface $slugger, ParameterBagInterface $params)
    {
        $this->slugger = $slugger;
        $this->params = $params;
    }

    public function upload(UploadedFile $file, FileImage $fileEntity)
    {
        $targetDirectory = $this->params->get('image_upload_directory');
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $newFilename = $safeFilename . '-' . uniqid();

        $fileEntity->setFilename($newFilename);
        $fileEntity->setExt($file->guessExtension());

        $filenameWithExt = $newFilename . '.' . $file->guessExtension();

        try {
            $file->move(
                $targetDirectory,
                $filenameWithExt
            );
        } catch (FileException $e) {
            return $e;
        }

        return $fileEntity;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}
