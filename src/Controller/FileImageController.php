<?php

namespace App\Controller;

use App\Entity\FileImage;
use App\Form\FileImageType;
use App\Repository\FileImageRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use App\Service\FileUploader;

/**
 * @Route("/file/image")
 */
class FileImageController extends AbstractController
{
    /**
     * @Route("/", name="app_file_image_index", methods={"GET"})
     */
    public function index(FileImageRepository $fileImageRepository): Response
    {
        return $this->render('file_image/index.html.twig', [
            'file_images' => $fileImageRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_file_image_new", methods={"GET", "POST"})
     */
    public function new(Request $request, FileUploader $fileUploader, FileImageRepository $fileImageRepository): Response
    {
        $fileImage = new FileImage();
        $form = $this->createForm(FileImageType::class, $fileImage);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            //on recupere les images transmises
            $image_upload = $form->get('image_upload')->getData();

            if ($image_upload) {
                $fileImage = $fileUploader->upload($image_upload, $fileImage);
                if ($fileImage instanceof FileException) {
                    $this->addFlash('danger', $fileImage->getMessage());
                    return $this->redirectToRoute('app_file_image_new');
                }

                $fileImageRepository->add($fileImage, true);
                return $this->redirectToRoute('app_file_image_index', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->renderForm('file_image/new.html.twig', [
            'file_image' => $fileImage,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_file_image_show", methods={"GET"})
     */
    public function show(FileImage $fileImage): Response
    {
        return $this->render('file_image/show.html.twig', [
            'file_image' => $fileImage,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_file_image_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, FileImage $fileImage, FileImageRepository $fileImageRepository): Response
    {
        $form = $this->createForm(FileImageType::class, $fileImage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fileImageRepository->add($fileImage, true);
            $fileImage = new FileImage();
            // /** @var UploadedFile $uploadedFile */
            // $uploadedFile = $form['fileImage']->getData();
            // $targetDirectory = $this->params->get('image_upload_directory');
            return $this->redirectToRoute('app_file_image_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('file_image/edit.html.twig', [
            'file_image' => $fileImage,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_file_image_delete", methods={"POST"})
     */
    public function delete(Request $request, FileImage $fileImage, FileImageRepository $fileImageRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $fileImage->getId(), $request->request->get('_token'))) {
            $fileImageRepository->remove($fileImage, true);
        }

        return $this->redirectToRoute('app_file_image_index', [], Response::HTTP_SEE_OTHER);
    }
}
