<?php

namespace App\Controller;

use App\Entity\Prestation;
use App\Form\PrestationType;
use App\Repository\PrestationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/presta/admin")
 */
class PrestaAdminController extends AbstractController
{
    /**
     * @Route("/", name="app_presta_admin_index", methods={"GET"})
     */
    public function index(PrestationRepository $prestationRepository): Response
    {
        return $this->render('presta_admin/index.html.twig', [
            'prestations' => $prestationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_presta_admin_new", methods={"GET", "POST"})
     */
    public function new(Request $request, PrestationRepository $prestationRepository): Response
    {
        $prestation = new Prestation();
        $form = $this->createForm(PrestationType::class, $prestation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $prestationRepository->add($prestation, true);

            return $this->redirectToRoute('app_presta_admin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('presta_admin/new.html.twig', [
            'prestation' => $prestation,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_presta_admin_show", methods={"GET"})
     */
    public function show(Prestation $prestation): Response
    {
        return $this->render('presta_admin/show.html.twig', [
            'prestation' => $prestation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_presta_admin_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Prestation $prestation, PrestationRepository $prestationRepository): Response
    {
        $form = $this->createForm(PrestationType::class, $prestation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $prestationRepository->add($prestation, true);

            return $this->redirectToRoute('app_presta_admin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('presta_admin/edit.html.twig', [
            'prestation' => $prestation,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_presta_admin_delete", methods={"POST"})
     */
    public function delete(Request $request, Prestation $prestation, PrestationRepository $prestationRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $prestation->getId(), $request->request->get('_token'))) {
            $prestationRepository->remove($prestation, true);
        }

        return $this->redirectToRoute('app_presta_admin_index', [], Response::HTTP_SEE_OTHER);
    }
}
