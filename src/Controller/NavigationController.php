<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Session\Session;

class NavigationController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function home(Session $session)
    {
        $return = [];

        if ($session->has('message')) {
            $message = $session->get('message');
            $session->remove('message'); //on vide la variable message dans la session
            $return['message'] = $message; //on ajoute à l'array de paramètres notre message
        }
        return $this->render('navigation/home.html.twig', $return);
    }
    /**
     *  * Nécessite juste d'être connecté
     * @Route("/membre", name="membre")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * fonctionne aussi avec ROLE_USER
     */
    public function membre(Session $session)
    {

        $return = [];

        if ($session->has('message')) {
            $message = $session->get('message');
            $session->remove('message'); //on vide la variable message dans la session
            $return['message'] = $message; //on ajoute à l'array de paramètres notre message
        }
        return $this->render('navigation/membre.html.twig', $return);
    }

    /**
     * Besoin des droits admin
     * @Route("/admin", name="admin")
     * @IsGranted("ROLE_ADMIN")
     */
    public function admin(Session $session)
    {
        //récupération de l'utilisateur security>Bundle
        $user = $this->getUser();
        //test si un utilisateur n'est pas connecté
        if (!$user) {
            $session->set("message", "Merci de vous connecter");
            return $this->redirectToRoute('app_login');
            //vérification des droits.
        } else if (in_array('ROLE_ADMIN', $user->getRoles())) {
            return $this->render('navigation/admin.html.twig');
        }
        //redirection
        $session->set("message", "Vous n'avez pas le droit d'acceder à la page admin vous avez été redirigé sur cette page");

        if ($session->has('message')) {
            $message = $session->get('message');
            $session->remove('message'); //on vide la variable message dans la session
            $return['message'] = $message; //on ajoute à l'array de paramètres notre message
        }
        return $this->redirectToRoute('home', $return);
    }
}
