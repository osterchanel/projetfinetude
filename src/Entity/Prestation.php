<?php

namespace App\Entity;

use App\Repository\PrestationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PrestationRepository::class)
 */
class Prestation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createAt;

    /**
     * @ORM\OneToMany(targetEntity=FileImage::class, 
     * mappedBy="prestation", orphanRemoval=true, cascade={"persist"})
     */
    private $fileImages;

    public function __construct()
    {
        $this->fileImages = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeImmutable $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * @return Collection<int, FileImage>
     */
    public function getFileImages(): Collection
    {
        return $this->fileImages;
    }

    public function addFileImage(FileImage $fileImage): self
    {
        if (!$this->fileImages->contains($fileImage)) {
            $this->fileImages[] = $fileImage;
            $fileImage->setPrestation($this);
        }

        return $this;
    }

    public function removeFileImage(FileImage $fileImage): self
    {
        if ($this->fileImages->removeElement($fileImage)) {
            // set the owning side to null (unless already changed)
            if ($fileImage->getPrestation() === $this) {
                $fileImage->setPrestation(null);
            }
        }

        return $this;
    }
}
